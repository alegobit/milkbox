package com.alegodroid.milkbox;
import com.alegodroid.milkbox.trans.*;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SectionPagerAdapter mSectionsPagerAdapter;
        CubeViewPager mViewPager;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSectionsPagerAdapter = new SectionPagerAdapter(getSupportFragmentManager());
        mViewPager = (CubeViewPager) findViewById(R.id.pager);

        InfinitePagerAdapter infinitePagerAdapter = new InfinitePagerAdapter(mSectionsPagerAdapter);
        mViewPager.setAdapter(infinitePagerAdapter);
        mViewPager.setPageTransformer(true, new CubeOutTransformer());
        mViewPager.setPageMargin(0);
        mViewPager.setPagingEnabled(true);
    }


}