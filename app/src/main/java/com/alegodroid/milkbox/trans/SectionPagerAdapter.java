package com.alegodroid.milkbox.trans;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.alegodroid.milkbox.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by aleGoKILL on 2/3/2018.
 */

public class SectionPagerAdapter extends FragmentPagerAdapter {
    private Map<Integer, String> mFragmentTags;
    private FragmentManager mFragmentManager;
    private Fragment1 m_fragment1;
    private Fragment2 m_fragment2;
    private Fragment3 m_fragment3;
    private Fragment4 m_fragment4;

    public SectionPagerAdapter(FragmentManager fm) {
        super(fm);
        mFragmentManager = fm;
        mFragmentTags = new HashMap<>();
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                if (m_fragment1 == null) {
                    m_fragment1 = new Fragment1();
                }
                return m_fragment1;
            case 1:
                if (m_fragment2 == null) {
                    m_fragment2 = new Fragment2();
                }
                return m_fragment2;
            case 2:
                if (m_fragment3 == null) {
                    m_fragment3 = new Fragment3();
                }
                return m_fragment3;
            case 3:
                if (m_fragment4 == null) {
                    m_fragment4 = new Fragment4();
                }
                return m_fragment4;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        Object obj = super.instantiateItem(container, position);

        if (obj instanceof Fragment) {
            // record the fragment tag here.
            Fragment f = (Fragment) obj;
            String tag = f.getTag();
            mFragmentTags.put(position, tag);
        }
        return obj;
    }
}
