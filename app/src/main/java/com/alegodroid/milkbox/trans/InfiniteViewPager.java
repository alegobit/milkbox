package com.alegodroid.milkbox.trans;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

/**
 * Created by aleGoKILL on 2/3/2018.
 */

public class InfiniteViewPager extends ViewPager {
    boolean fakePositionWasSet;

    public InfiniteViewPager(Context context) {
        super(context);
    }

    public InfiniteViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setAdapter(PagerAdapter adapter) {
        super.setAdapter(adapter);
        // offset first element so that we can scroll to the left
        setCurrentItem(0);
    }

    @Override
    public void setCurrentItem(int item) {
        // offset the current item to ensure there is space to scroll
        setCurrentItem(item, false);
    }

    @Override
    public void setCurrentItem(int item, boolean smoothScroll) {
        if (getAdapter().getCount() == 0) {
            super.setCurrentItem(item, smoothScroll);
            return;
        }
        if (getAdapter() instanceof InfinitePagerAdapter) {
            InfinitePagerAdapter infAdapter = (InfinitePagerAdapter) getAdapter();
            int currentPositionInAdapter = super.getCurrentItem();
            //shift current start position
            if (!fakePositionWasSet) {
                item += getOffsetAmount();
                fakePositionWasSet = true;
            }
            int currentPositionInRealAdapter = getCurrentItem();
            if (currentPositionInRealAdapter == 0 && item == infAdapter.getRealCount() - 1) {
                item = currentPositionInAdapter - 1;
            } else if (currentPositionInRealAdapter == infAdapter.getRealCount() - 1 && item == 0) {
                item = currentPositionInAdapter + 1;
            } else {
                item = currentPositionInAdapter + (item - currentPositionInRealAdapter);
            }
        } else {
            item = getOffsetAmount() + (item % getAdapter().getCount());
        }
        super.setCurrentItem(item, smoothScroll);
    }

    @Override
    public int getCurrentItem() {
        if (getAdapter().getCount() == 0) {
            return super.getCurrentItem();
        }
        int position = super.getCurrentItem();
        if (getAdapter() instanceof InfinitePagerAdapter) {
            InfinitePagerAdapter infAdapter = (InfinitePagerAdapter) getAdapter();
            // Return the actual item position in the data backing InfinitePagerAdapter
            return (position % infAdapter.getRealCount());
        } else {
            return super.getCurrentItem();
        }
    }

    private int getOffsetAmount() {
        if (getAdapter().getCount() == 0) {
            return 0;
        }
        if (getAdapter() instanceof InfinitePagerAdapter) {
            InfinitePagerAdapter infAdapter = (InfinitePagerAdapter) getAdapter();
            // allow for 100 back cycles from the beginning
            // should be enough to create an illusion of infinity
            // warning: scrolling to very high values (1,000,000+) results in
            // strange drawing behaviour
            return infAdapter.getRealCount() * 400;
        } else {
            return 0;
        }
    }
}
