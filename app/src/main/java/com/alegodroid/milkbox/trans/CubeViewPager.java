package com.alegodroid.milkbox.trans;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by aleGoKILL on 2/3/2018.
 */

public class CubeViewPager extends InfiniteViewPager {
    private boolean mEnabled;
    public CubeViewPager(Context context) {
        super(context);
    }

    public CubeViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent arg0) {
        return mEnabled ? super.onInterceptTouchEvent(arg0) : false;
    }

    public void setPagingEnabled(boolean enabled) {
        mEnabled = enabled;
    }
}
